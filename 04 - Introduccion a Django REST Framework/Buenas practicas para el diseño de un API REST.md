
---
# **Buenas practicas para el diseño de un API REST**

## **Perspectiva**

Tenemos que ponernos en una perspectiva de un usuario de un API y no el el diseñador de un API.

## **Goal**

Objetivo final, que problema tiemos que resolverle al usuario final.

## **Success**

El exito se mide a que tan rapido y que tan facil puede ponerse a usar su API.

## **REST**

Es un estilo de arquitectura.

## **Resources**

Usa sustantivos, no verbos!

Ej. Esto es una mala practica

```sh
/getAllmMovies
/getMovies/<id>
/addMovie
/deleteMovie
/updateMovie/<id>

/updateMovieTitle/<id>
/updateMovieDirector/<id>
/updateMovieReleaseDate/<id>
/updateMovieDirectors/<id>
...
```

Ej. Podemos resumirlo a esto con los methodos http.

```sh
/movies
/movies/<id>
```

```sh
GET /movies - list movies
POST /movies - create a movie
PUT /movies/<id> - updates movies
PATCH /movies/<id> - partially updates a movie 
DELETE /movies/<id> - removes a movies
```
## **Plurals**

Usar plurales ej. movies en vez de movie.

## **Relationships**

Ten cuidado con las relaciones anidadas.

Si un recurso solo puede salir dentro de otro recurso

```sh
GET /team/<id>/players - lists all players of an specific team
GET /team/<id>/players/<id> - retrieves player of a team
POST /team/<id>/players - creates a new player in a team
PUT /team/<id>/players/<id> - updates player of a team
DELETE /team/<id>/players/<id> - removes a player from a team
```

Una buena practica es que no crescamos el nivel de profundidad a mas de dos niveles.

## **Use '?'**

Otra buena practica es usar los paramatros en la url, de esta manera podemos filtrar datos o pondemos realizar busquedas mas complejas, sin tener que meter que meter esas necesidades en la url.

```sh
GET /user?age=18&city=SFO
```

## **Filtering**

Filtrar por campos, i.e (Id est, esto es):

```sh
GET /movies?status=released
```

## **Sorting**

```sh
GET /movies?sort=release_date, updated_at
```

## **Searching**

```sh
GET /movies?q=some_ewesome_query_text
```

## **Aliases**

Para queries comunes, i.e:

```sh
GET /movies/released
```

## **Fields**

Limitar los campos que contiene la respuesta de la API. Esto significa que la respuesta esta limitada por que nosotros queramos.

```sh
GET /movies?fields=id,directors,status
```

## **Pagination**

Limita el numero de respuesta por peticion.

```sh
GET /movies - 200 OK
{
    "status": 200,
    "results": [],
    "next": "/movies?page=4",
    "previous": null,
}
```

```sh
GET /movies?page=3 - 200 OK
{
    "status": 200,
    "results": [],
    "next": "/movies?page=4",
    "previous": "/movies?page=2",
}
```

## **Pagination with Metadata**

```sh
GET /movies?page=3 - 200 OK
{
    "status": 200,
    "results": [],
    "total": 94658,
    "next": "/movies?page=4",
    "previous": "/movies?page=2",
}
```

## **HTTP Status codes**

Una cosa importante es decirle al usuario que esta pasando del lado del servidor o indicale que esta sucediendo con su respuesta a traves de estados.

```sh
200 - Ok
400 - Bad request - Tu hiciste algo mal, tu peticion fue erronea.
500 - Internal Server Error - Algo malo sucedio en el servidor y no puedo responder tu solicitud.
```

* Otros:

```sh
201 - Created
304 - Not modified - No paso nada.
404 - Not found - No se encontro.
401 - Unauthorized - No esta autorizado
403 - Forbidden - Prohibida
```

## **Return resource**

Otra cosa importante es que cuando estemos creando un objeto, para evitar que el cliente tenga que hacer otra peticion y buscar ese objeto, tienes que regresar el objeto. Esto es tanto para el front como para el back porque vamos a estar haciendo menos peticiones.}

```sh
POST /movies - 201 Created
{
    "id": 1,
    "title": "Inception",
    "status": "released"
}
```

## **Handler errors**

Importante decirle al cliente que fallo. Por ejemplo al llenar un formulario, excediste el largo de un campo o un email no valido, etc.

## **Simple messages**

```sh
{
    "code": 845,
    "message": "Invalid access token",
    "description": "The provided access token has expired"
}
```
O podemos hacer cosas mas complejas

```sh
{
    "code": 438,
    "message": "Validation failed",
    "errors": [
        {
            "code": 123,
            "field": "title",
            "message": "title length must be less than 50"
        }
    ]
}
```

## **Versioning**

Especifica la version sobre la url, i.e:

```sh
GET /v1/movies
```

## **Subdomains**

No mezclar cliente con el back, se suelen usar subdominios i.e:

```sh
api.gatos.io
```

## **Stability & Consistency**

Las respuestas deben ser estables y consistentes.

## **JSON First**

Establecer el tipo en la URL siempre que sea posible. i.e:

```sh
GET /movies?type=json
```

## **camelCase vs snake_case**

snake_case es 20% mas facil de leer que camelCase!

## **Authentication**

* Stateless : Un proceso o una aplicación sin estado se refiere a los casos en que estos están aislados. No se almacena información sobre las operaciones anteriores ni se hace referencia a ellas. Cada operación se lleva a cabo desde cero, como si fuera la primera vez. Las aplicaciones sin estado proporcionan un servicio o una función y usan servidores de impresión, de red de distribución de contenido (CDN) o web para procesar estas solicitudes a corto plazo. 

Las API restfull deben ser stateless. Los tokens de acceso siempre son una buena idea.

## **Browser**

Importante realizar pruebas sobre el o los navegadores para ver si todo corre como lo desarrollado.

## **Document your API!**

"Una cosa mas frustrante que tener que escribir una API, es tener que usar una API sin documentacion"

## **Pro Tips**

* SSL : No podes no usar SSL.
```sh
SSL es el acrónimo de Secure Sockets Layer (capa de sockets seguros), la tecnología estándar para mantener segura una conexión a Internet, así como para proteger cualquier información confidencial que se envía entre dos sistemas e impedir que los delincuentes lean y modifiquen cualquier dato que se transfiera, incluida información que pudiera considerarse personal. Los dos sistemas pueden ser un servidor y un cliente (por ejemplo, un sitio web de compras y un navegador) o de servidor a servidor (por ejemplo, una aplicación con información que puede identificarse como personal o con datos de nóminas).

HTTPS (Hyper Text Transfer Protocol Secure o protocolo seguro de transferencia de hipertexto) aparece en la dirección URL cuando un sitio web está protegido por un certificado SSL. Los detalles del certificado, por ejemplo la entidad emisora y el nombre corporativo del propietario del sitio web, se pueden ver haciendo clic en el símbolo de candado de la barra del navegador.
```

* Cache : No vayas a preguntar las mismas cosas que son costosos, multiples veces, ponlas en una capa de cache.

* Validate : Valida todo tus datos, siempre. El cliente puede enviar cualquier tipo de datos y se tiene que estar listo para responder ese tipo de request.

* CSRF : Protegete ante ataques de CSRF

```sh
Cross Site Request Forgery (CSRF o XSRF) es un tipo de ataque que se suele usar para estafas por Internet. Los delincuentes se apoderan de una sesión autorizada por el usuario (session riding) para realizar actos dañinos. El proceso se lleva a cabo mediante solicitudes HTTP.
```

* Limit the request

* Completed with a SDK
