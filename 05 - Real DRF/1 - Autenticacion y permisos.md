
---
# Autenticacion

Es la parte de asociar una peticion a un usuario. Una vez que sucede esto al objeto request se le asignan dos propiedades: 

- request.user : Es una instancia del modelo de usuario. user.contrib.auth (En nuestro caso tenemos un usuario custom, que no es el usuario por default de django)
- request.auth : No tiene un formato, la podemos configurar a lo que queramos. Tiene informacion del metodo de autenticacion.

Si no hay una clase autenticacion o si no hay una autenticacion, el request.user se va a configurar a django.contrib.auth.models.AnonymousUser, y 
request.auth se va a setear a **none**.

## Configuración del esquema de autenticación

Por default el esquema de autenticacion de django incluyen estas dos.

```sh
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ]
}
```

Al igual que en los parsers y en los renderers, todo el rest-framework lo podemos definir en un comportamiento global, pero tambien podemos definir comportamientos por vista.
Entonces por ejemplo podemos tener una vista que particularmente diga, 'yo se que en toda la aplicacion se acepta autenticacion por token pero en esta vista quiero que tambien se acepte autenticacion por session o basic'.

## Respuestas no autorizadas y prohibidas

Cuando se lanza un error de autenticacion, lanza 2 tipos de respuesta:

* 401 - Unauthorized - Tu peticion no pudo ser asociada a un usuario.
* 403 - Permission Denied - Cuando si se asocia a un usuario pero no tiene permiso de acceder.

## Tipos de autenticacion

* **BasicAuthentication** : Se envia la palabra basic, se envia las credenciales.

```sh
Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ== 
```

tambien podemos en vez de enviar un token, podemos enviar al username. Po lo general todos los headers, si usar SSL van encriptados, entonces no hay problemas con esto.

* **Session** : Va en contra de nuestros principios de rest (las sesiones no son stateless) entonces no nos sirve.

* **TokenAuthentication** : Se refiere a un conjunto de caracteres unico que te identifica y te da permiso. Django-rest-framework provee por default una generacion de tokens.

```sh
INSTALLED_APPS = [
    ...
    'rest_framework.authtoken'
]
```

* **OAuth** : Es un protocolo para que una aplicacion le de acceso a sus datos a otra aplicacion. No es de usuarios.

---
## JSON Web Token Authentication
```sh
Es un standar que resuelve el problema de tener que almacenar una referencia de los token de afuera para que cuando llegen nosotros podamos identificar como un usuario. Esto quiere decir, te llega un codigo y cuando te llega ese codigo como sabes a que usuario pertenece. Lo que dice JWT es que el token debe ser capas de almacenar la informacion del token, pero ademas tiene que ser capas de que nadie pueda ser capas de manipular ese token en el camino y crear acceder porque si. Tu en la base de datos no tienes que tener referencia a ellos. 
JWT lo que hace es un conjunto de datos, firma criptografica y algoritmo que esta utilizando.
```
![JSON Web Token Authentication](img/jwt.png "JWT")
```sh
Tenemos el token codificado (izq) y decodificado (der).
La parte decoficada tiene 3 partes. El header, el payload y la firma. (Ver que la codificacion esta dada en la seccion respectiva al color).
Cuando llega, tienes una manera de verificar esa firma criptografica y no necesitas guardarla. El problema es que no tienes control sobre esos tokens. Si de repente dices este token dura 30 dias. Pero detectas que ese token esta siendo mal utilizado o utilizado en contra de tus politicas, no tienes control sobre ese token.
Nosotros lo vamos a utilizar para la verificacion de un email. Generamos un token, se lo damos al usuario y sin tener que guardar en la base de datos lo podemos revisar y ver si aun es valido.
```

---
# Permisos

Permissions es otorgado por authentication y throttling (cuantas peticiones por frame de tiempo pueden acceder a esa vista, por ejemplo 1 por dia, 1 por segundo, 30 por segundo, etc.)

Los permisos se encargan de determinar la parte de, una vez que esta autorizado el usuario, ¿Este usuario tiene permiso de acceder a esta vista?
Ese permiso puede ser definido por los atributos del usuario, por ej. si es administrador, usuario normal o por el tipo de objeto que esta accediendo por ej. este usuario puede acceder a ese circulo (modelo de la aplicacion cride)

## Configuración de la política de permisos

La política de permisos predeterminada se puede configurar globalmente, usando la DEFAULT_PERMISSION_CLASSESconfiguración. Por ejemplo.

```sh
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ]
}
```

Si no se especifica, esta configuración predeterminada permite el acceso sin restricciones:

```sh
'DEFAULT_PERMISSION_CLASSES': [
   'rest_framework.permissions.AllowAny',
]
```

También puede configurar la política de autenticación por vista o por conjunto de vista, utilizando las APIViewvistas basadas en clases.

```sh
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

class ExampleView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        content = {
            'status': 'request was permitted'
        }
        return Response(content)
```

O, si está utilizando el @api_viewdecorador con vistas basadas en funciones.

```sh
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def example_view(request, format=None):
    content = {
        'status': 'request was permitted'
    }
    return Response(content)
```

## ¿ Como definimos un permiso ?

Todos los permisos heredan de BasePermission y tiene dos metodos has_permission (permiso general) y has_object_permission (permiso por objeto).
has_object_permission recibe el objeto dentro del metodo y tu puedes manipular ese metodo para customizar los permisos.

## Permisos de django

* **AllowAny** : Permite todos.
* **IsAutheticated** : No le importa quien seas, solo que estes identificado
* **IsAdminUser** : Que seas usuario. Lo hace contra user.is_staff.
* **IsAutheticatedOrReadOnly** : Solo puede usar los metodos seguros, esto es GET, HEAD o OPTIONS.
* **DjangoModelPermissions** : Puedo usar el sistema de permisos de django y aplicarlo contra un objeto.
* **Otros...**
